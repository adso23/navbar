
import React from 'react';
import {Section} from './section';
import contactBackground from '../img/contact.jpg';

export const Contact = () => {
  return (
    <Section id="contact" title="Contact" className="home-section">
     <div className="content">
        
        <img src={contactBackground} alt="tech" className="home-section"/>
        </div>
    </Section>
  );
};


