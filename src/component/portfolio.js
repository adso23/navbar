
import React from 'react';
import {Section} from './section';
import portfolioBackground from '../img/ferreSys.png';

export const Portfolio = () => {
  return (
    <Section id="portfolio" title="MY PROJECTS">
      <div className="content">
        
        <img src={portfolioBackground} alt="ferreSys" />
        </div>
    </Section>
  );
};


