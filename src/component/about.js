
import React from 'react';
import {Section} from './section';
import aboutBackground from '../img/logoPanda.png'; 

export const About = () => {
  return (
    <Section id="about" title="About Me" className="about-section">
     
      <div className="content">
        
        <img src={aboutBackground} alt="Logo Panda" />
        <h2>¡Hola, soy [Yocser Chavez]!</h2>
        <p>
          Soy un apasionado tecnólogo en análisis y desarrollo de software con experiencia en la creación de soluciones innovadoras. Mi enfoque principal es el desarrollo web, donde he cultivado habilidades sólidas en tecnologías clave.
        </p>
        <p>
          Mi experiencia técnica abarca el trío fundamental de desarrollo web: JavaScript (ES6+), HTML5 y CSS3. Además, tengo un profundo conocimiento y experiencia en el desarrollo de aplicaciones modernas utilizando React.js, así como la gestión eficiente de bases de datos utilizando PostgreSQL.
        </p>
        <p>
          En el lado del servidor, me desenvuelvo con Node.js, construyendo aplicaciones robustas y escalables. Mi enfoque en la creación de aplicaciones completas me ha permitido participar en proyectos que van desde sitios web dinámicos hasta aplicaciones de una sola página (SPA).
        </p>
        <p>
          Más allá de mis habilidades técnicas, me enorgullece destacar mis habilidades blandas. Soy un comunicador efectivo, capaz de colaborar en equipos multifuncionales, y tengo una actitud positiva y proactiva para abordar desafíos. Siempre estoy buscando oportunidades para aprender y mejorar mis habilidades, manteniéndome actualizado con las últimas tendencias tecnológicas.
        </p>
        <p>
          ¡Estoy emocionado por las oportunidades que el mundo del desarrollo de software tiene para ofrecer y espero contribuir significativamente en proyectos futuros!
        </p>
      </div>
    </Section>
  );
};




