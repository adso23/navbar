
import React, { useState } from 'react';
import { Link } from 'react-scroll';
import logoImage from '../img/logoPanda.png';
export const Navbar = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  return (
    <nav className="navbar">
      <div className="logo">
         <img src={logoImage} alt="Logo"  />
      </div>
      <div className={`menu ${isOpen ? 'open' : ''}`}>
        <Link to="home" spy={true} smooth={true} offset={-70} duration={500}>Home</Link>
        <Link to="about" spy={true} smooth={true} offset={-70} duration={500}>About</Link>
        <Link to="portfolio" spy={true} smooth={true} offset={-70} duration={500}>Portfolio</Link>
        <Link to="contact" spy={true} smooth={true} offset={-70} duration={500}>Contact</Link>
      </div>
      <div className="menu-icon" onClick={toggleMenu}>
        <div className={`bar ${isOpen ? 'open' : ''}`}></div>
        <div className={`bar ${isOpen ? 'open' : ''}`}></div>
        <div className={`bar ${isOpen ? 'open' : ''}`}></div>
      </div>
    </nav>
  );
};







