
import React from 'react';
import { Navbar } from './component/nav';
import {Home} from './component/home'; 
import {About} from './component/about'; 
import {Portfolio} from './component/portfolio'; 
import {Contact} from './component/contact';
import './css/styles.css';

export const App = () => {
  return (
    <div>
      <Navbar />
      <Home />
      <About />
      <Portfolio />
      <Contact />
    </div>
  );
};



